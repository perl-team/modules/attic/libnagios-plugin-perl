libnagios-plugin-perl (0.36-3) UNRELEASED; urgency=medium

  [ Salvatore Bonaccorso ]
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.
  * Remove Ryan Niebur from Uploaders. Thanks for your work!

 -- Salvatore Bonaccorso <carnil@debian.org>  Sat, 30 Jan 2016 20:05:38 +0100

libnagios-plugin-perl (0.36-2) unstable; urgency=low

  * Team upload.

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ Axel Beckert ]
  * debian/copyright: migrate pre-1.0 format to 1.0 using "cme fix dpkg-
    copyright"

  [ gregor herrmann ]
  * Strip trailing slash from metacpan URLs.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend

  [ gregor herrmann ]
  * Add patch to fix "defined(%hash)" error. (Closes: #789343)
  * Mark package as autopkgtest-able.
  * Declare compliance with Debian Policy 3.9.6.
  * Bump debhelper compatibility level to 9.

 -- gregor herrmann <gregoa@debian.org>  Sat, 20 Jun 2015 02:43:00 +0200

libnagios-plugin-perl (0.36-1) unstable; urgency=low

  [ Ansgar Burchardt ]
  * debian/control: Convert Vcs-* fields to Git.

  [ Salvatore Bonaccorso ]
  * debian/copyright: Replace DEP5 Format-Specification URL from
    svn.debian.org to anonscm.debian.org URL.
  * Imported Upstream version 0.36
  * Bump Standards-Version to 3.9.2
  * Update debian/copyright file for debian/* packaging.
    Add myself to debian/* packaging stanza.

 -- Salvatore Bonaccorso <carnil@debian.org>  Fri, 23 Dec 2011 12:50:39 +0100

libnagios-plugin-perl (0.35-1) unstable; urgency=low

  [ Ansgar Burchardt ]
  * Update my email address.

  [ Salvatore Bonaccorso ]
  * New upstream release
  * Add myself to Uploaders.
  * Refresh debian/copyright:
    - Explicitly refer to GPL-1 licence text in common-licenses.
    - Refer to Debian systems in general instead of only Debian GNU/Linux
      systems. 
  * Bump Standards-Version to 3.9.1.
  * Drop fix-pod2man-errors patch at it was applied upstream.

 -- Salvatore Bonaccorso <carnil@debian.org>  Sat, 04 Dec 2010 14:58:41 +0100

libnagios-plugin-perl (0.34-1) unstable; urgency=low

  [ Nathan Handler ]
  * debian/watch: Update to ignore development releases.

  [ Salvatore Bonaccorso ]
  * debian/control: Changed: Replace versioned (build-)dependency on
    perl (>= 5.6.0-{12,16}) with an unversioned dependency on perl (as
    permitted by Debian Policy 3.8.3).

  [ Ryan Niebur ]
  * Update ryan52's email address

  [ Ansgar Burchardt ]
  * New upstream release.
  * Use source format 3.0 (quilt); drop README.source and quilt framework.
  * debian/copyright: Minor formatting changes for current DEP-5 proposal.
  * Use tiny debian/rules.
  * Bump Standards-Version to 3.8.4.
  * Add myself to Uploaders.

 -- Ansgar Burchardt <ansgar@43-1.org>  Sat, 17 Apr 2010 16:26:11 +0900

libnagios-plugin-perl (0.33-1) unstable; urgency=low

  * New upstream release
  * Debian Policy 3.8.1
  * Add debian/README.source to document quilt usage, as required by
    Debian Policy since 3.8.0.

 -- Ryan Niebur <ryanryan52@gmail.com>  Fri, 05 Jun 2009 16:43:16 -0700

libnagios-plugin-perl (0.32-1) unstable; urgency=low

  * Initial Release. (Closes: #491835)

 -- Ryan Niebur <ryanryan52@gmail.com>  Sat, 07 Mar 2009 09:28:11 -0800
