Source: libnagios-plugin-perl
Maintainer: Debian Perl Group <pkg-perl-maintainers@lists.alioth.debian.org>
Uploaders: Ansgar Burchardt <ansgar@debian.org>,
           Salvatore Bonaccorso <carnil@debian.org>
Section: perl
Testsuite: autopkgtest-pkg-perl
Priority: optional
Build-Depends: debhelper (>= 9)
Build-Depends-Indep: libclass-accessor-perl,
                     libconfig-tiny-perl,
                     libmath-calc-units-perl,
                     libparams-validate-perl,
                     perl
Standards-Version: 3.9.6
Vcs-Browser: https://anonscm.debian.org/cgit/pkg-perl/packages/libnagios-plugin-perl.git
Vcs-Git: https://anonscm.debian.org/git/pkg-perl/packages/libnagios-plugin-perl.git
Homepage: https://metacpan.org/release/Nagios-Plugin

Package: libnagios-plugin-perl
Architecture: all
Depends: ${misc:Depends},
         ${perl:Depends},
         libclass-accessor-perl,
         libconfig-tiny-perl,
         libmath-calc-units-perl,
         libparams-validate-perl
Description: family of perl modules to streamline writing Nagios
 Nagios::Plugin and its associated Nagios::Plugin::* modules are a family of
 perl modules to streamline writing Nagios plugins. The main end user modules
 are Nagios::Plugin, providing an object-oriented interface to the entire
 Nagios::Plugin::* collection, and Nagios::Plugin::Functions, providing a
 simpler functional interface to a useful subset of the available
 functionality.
 .
 The purpose of the collection is to make it as simple as possible for
 developers to create plugins that conform the Nagios Plugin guidelines
 (http://nagiosplug.sourceforge.net/developer-guidelines.html).
